Sutty .env create
=================

Creates a `.env` file from a template.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "role"
    include_role:
      name: "sutty_dotenv_create"
```

License
-------

MIT-Antifa
